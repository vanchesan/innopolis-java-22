package realworld;

import java.io.IOException;

public class UserService {

    private UsersRepository usersRepository;

    public UserService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public int countOfAllUsers() {
        return usersRepository.findAll().size();
    }
}
