import java.util.Arrays;
import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        int[] array = {2, 4, 10, 11, 12, 13};

        boolean exists = false;

        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (number < array[middle]) {
                right = middle - 1;
            } else if (number > array[middle]) {
                left = middle + 1;
            } else {
                exists = true;
                break;
            }

            middle = left + (right - left) / 2;
        }

        // exists = true, false
        if (exists) {
            System.out.println("Число в массиве есть!");
        } else {
            System.out.println("Числа в массиве нет!");
        }

    }
}
