package example01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            throw new IllegalStateException(e);
//        }
//        Thread mainThread = Thread.currentThread();

        EggThread eggThread = new EggThread();
        HenThread henThread = new HenThread();


        eggThread.start();
        henThread.start();

        try {
            eggThread.join();
            henThread.join();
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }

        for (int i = 0; i < 100; i++) {
            System.out.println("Human");
        }
    }
}