import java.util.*;

public class Main3 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        Set<String> linkedSet = new LinkedHashSet<>();
        Set<String> sortedSet = new TreeSet<>();

        linkedSet.add("Привет");
        linkedSet.add("Привет");
        linkedSet.add("Пока");
        linkedSet.add("Армавир");

        for (String s : linkedSet) {
            System.out.println(s);
        }
    }
}
