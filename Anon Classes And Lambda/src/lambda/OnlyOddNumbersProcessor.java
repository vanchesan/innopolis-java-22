package lambda;

public class OnlyOddNumbersProcessor implements NumbersProcessor {

    @Override
    public int process(int number) {
        // если число нечетное
        if (number % 2 == 1) {
            // возвращаем это число
            return number;
        } else {
            // если оно четное, возвращаем 0
            return 0;
        }
    }
}
