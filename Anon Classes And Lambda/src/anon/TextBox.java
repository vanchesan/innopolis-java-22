package anon;

public abstract class TextBox {
    // поле, которое хранит текст, введенный пользователем
    protected String text;

    /**
     * Метод для ввода текста
     * @param text текст, который ввел пользователь
     */
    public void enterText(String text) {
        // сначала отправляем строку в метод beforeEnterText, чтобы этот метод как-то изменил строку
        // измененную строку положил в processedText
        String processedText = beforeEnterText(text);
        // кладем измененную строку в поле
        this.text = processedText;
        // вызываем метод, который срабатывает после ввода текста
        afterEnterText();
    }

    /**
     * Метод, который вызывается перед тем, как сохранить строку
     * и может ее как-то предварительно изменить
     * @param text введенный пользователем текст
     * @return измененный текст
     */
    public abstract String beforeEnterText(String text);

    /**
     * Метод следует вызывать после того, как текст был введен
     */
    public abstract void afterEnterText();


}
