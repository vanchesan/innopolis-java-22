
public class Main {

    public static int hashCode(char[] value) {
        int h = 0;
        for (int i = 0; i < value.length; i++) {
            h = 31 * h + value[i];
        }
        return h;
    }
    // 101, 72, 108, 108, 111, 33

    // h = 0, i = 0, value[0] = 101
    // h = 31 * 0 + 101

    // h = 31 * 0 + 101, i = 1, value[1] = 72
    // h = 31 * (31 * 0 + 101) + 72

    // h = 31 * (31 * 0 + 101) + 72, i = 2, value[2] = 108
    // h = 31 * (31 * (31 * 0 + 101) + 72) + 108

    // h = 31 * (31 * (31 * 0 + 101) + 72) + 108, i = 3, value[3] = 108
    // h = 31 * (31 * (31 * (31 * 0 + 101) + 72) + 108) + 108

    // h = 31 * (31 * (31 * (31 * 0 + 101) + 72) + 108) + 108, i = 4, value[4] = 111
    // h = 31 * (31 * (31 * (31 * (31 * 0 + 101) + 72) + 108) + 108) + 111

    // h = 31 * (31 * (31 * (31 * (31 * 0 + 101) + 72) + 108) + 108) + 111, i = 5, value[5] = 33
    // h = 31 * (31 * (31 * (31 * (31 * (31 * 0 + 101) + 72) + 108) + 108) + 111) + 33

    public static int badHashCode(char[] value) {
        int hash = 0;
        for (int i = 0; i < value.length; i++) {
            hash += value[i];
        }
        return hash;
    }


    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Марсель", 28);
        map.put("Виктор", 25);
        map.put("Максим", 26);
        map.put("Даниил", 23);
        map.put("Разиль", 22);
        map.put("Айрат", 24);
        map.put("Тимур", 21);
        map.put("Михаил", 27);
        map.put("Разиль", 30);

        Set<String> set = new HashSet<>();
        set.put("Марсель");
        set.put("Марсель");
        set.put("Разиль");

        System.out.println(set.contains("Марсель"));
        System.out.println(set.contains("Разиль"));
        System.out.println(set.contains("Виктор"));

        Map<Human, Integer> humans = new HashMap<>();
    }
}