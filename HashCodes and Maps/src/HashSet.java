public class HashSet<V> implements Set<V> {

    private static final Object PRESENT = new Object();
    private HashMap<V, Object> map = new HashMap<>();

    @Override
    public void put(V v) {
        map.put(v, PRESENT);
    }

    @Override
    public boolean contains(V v) {
        return map.containsKey(v);
    }
}
