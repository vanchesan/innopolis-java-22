package example03;

public class Main {
    public static void main(String[] args) {
        Nokia nokia = new Nokia();
        iPhone iPhone = new iPhone();

        Cover<iPhone> coverForIPhone = new Cover<>(iPhone);
        Cover<Nokia> coverForNokia = new Cover<>(nokia);

        Nokia nokiaFromCover = coverForNokia.getPhone();
        iPhone iPhoneFromCover = coverForIPhone.getPhone();

        nokiaFromCover.call();
        iPhoneFromCover.photo();
    }
}