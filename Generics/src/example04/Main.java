package example04;

public class Main {
    public static void main(String[] args) {
        // boxing - упаковка примитивного значения в его оберточный тип
        Integer i = Integer.valueOf(10);
        // unboxing - получение примитивного значения из оберточного
        int i2 = i.intValue();

        // autoboxing - автоупаковка
        Integer x = 10;
        // autounboxing - автораспаковка
        int x1 = x;

        // char - Character
        // boolean - Boolean
        // и т.д.
        Cover<Integer> integerCover = new Cover<>(10);
        int value = integerCover.getPhone();

    }
}
