package example05;

public class NumbersContainer<T extends Number> {
    private static final int DEFAULT_SIZE = 5;

    private T[] elements;
    private int count;

    public NumbersContainer() {
        this.elements = (T[])new Object[DEFAULT_SIZE];
        this.count = 0;
    }

    public void add(T element) {
        this.elements[count] = element;
        count++;
    }

    public T get(int index) {
        return this.elements[index];
    }
}
