* Generic - механизм Java, который позволяет работать с типами в терминах "неизвестных типов"
* Правило - "не важно, какого типа a и b, важно, что они гарантированно одного типа"
* Компилятор выполняет предварительную проверку, что все типы используются верно
* Затем, если код использован правильно - буква T "стираются" - стирание типов
* Классы-обертки над примитивными типами - используются в Generic